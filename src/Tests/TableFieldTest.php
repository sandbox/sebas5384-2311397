<?php

/**
 * @file
 * Definition of Drupal\tablefield\Tests\tablefieldTest.
 */

namespace Drupal\tablefield\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Test the user-facing menus in Block Example.
 *
 * @ingroup tablefield
 */
class TableFieldTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('tablefield');
  
  protected $administratorAccount;
  protected $field_label;
  protected $field_type;
  
  /**
   * The installation profile to use with this test.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return array(
      'name' => 'Table Field Test',
      'description' => 'Test if the widget is available in the manage fields tab.',
      'group' => 'TableField',  
    );
  }
  
  /**
   * Enable modules and create user with specific permissions.
   */
  public function setUp() {
    parent::setUp();
    // Create user.
    $permissions = array(
      'administer content types',
      'administer node fields',
      'administer node form display',
    );
    $this->administratorAccount = $this->drupalCreateUser($permissions);

    $this->field_label = 'Ingredients';
    $this->field_type = 'tablefield';

  }
  
  /**
   * Tests tablefield.
   */
  
  public function testTableFieldAvailability() {
    // Login the admin user.
    $this->drupalLogin($this->administratorAccount);
    $theme_name = \Drupal::config('system.theme')->get('default');
    
    // Checks the module exists
    $this->assertTrue(module_exists('tablefield'));
    
    // Access to admin page
    // $this->drupalGet('admin/config/regional/language/icons');
    // $this->assertResponse(200, 'Description page exists.');
    
    // Test the add tab.edit-submit
    // Add the new entry.
    $this->drupalPostForm(
      'admin/structure/types/manage/page/fields',
      array(
        'fields[_add_new_field][type]' => $this->field_type,
        'fields[_add_new_field][field_name]' => 'ingredients',
        'fields[_add_new_field][label]' => $this->field_label,
      ),
      t('Save')
    );
    $this->assertRaw(t('These settings apply to the %label field everywhere it is used.', array('%label' => $this->field_label)), 'Field settings page was displayed.');
    
  }

}
