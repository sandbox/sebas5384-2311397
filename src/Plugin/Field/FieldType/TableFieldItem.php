<?php

/**
 * @file
 * Contains \Drupal\tablefield\Plugin\Field\FieldType\TableFieldItem.
 */

namespace Drupal\tablefield\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'tablefield' field type.
 *
 * @FieldType(
 *   id = "tablefield",
 *   label = @Translation("Table Field"),
 *   module = "tablefield",
 *   description = @Translation("Stores a table of text fields."),
 *   default_widget = "tablefield",
 *   default_formatter = "default"
 * )
 */
class TableFieldItem extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ),
        'format' => array(
          'type' => 'varchar',
          'length' => 255,
          'default value' => '',
        ),
      ),
      'indexes' => array(
        'format' => array('format'),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {

    // @todo, is this the best way to mark the default value form?
    // if we don't, it won't save the number of rows/cols
    // Allow the system settings form to have an emtpy table
    // $arg0 = arg(0);
    // if ($arg0 == 'admin') {
    //   return FALSE;
    // }

    // // Remove the preference fields to see if the table cells are all empty
    // unset($item['tablefield']['rebuild']);
    // unset($item['tablefield']['import']);
    // if (!empty($item['tablefield'])) {
    //   foreach ($item['tablefield'] as $cell) {
    //     if (!empty($cell)) {
    //       return FALSE;
    //     }
    //   }
    // }

    // return TRUE;


    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Table field value'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array &$form, array &$form_state, $has_data) {

    $element = array();
    $element['export'] = array(
      '#type' => 'checkbox',
      '#title' => 'Allow users to export table data as CSV',
      '#default_value' => $this->getSetting('export'),
    );
    $element['restrict_rebuild'] = array(
      '#type' => 'checkbox',
      '#title' => 'Restrict rebuilding to users with the permission "rebuild tablefield"',
      '#default_value' => $this->getSetting('restrict_rebuild'),
    );
    $element['lock_values'] = array(
      '#type' => 'checkbox',
      '#title' => 'Lock table field defaults from further edits during node add/edit.',
      '#default_value' => $this->getSetting('lock_values'),
    );
    $element['cell_processing'] = array(
      '#type' => 'radios',
      '#title' => t('Table cell processing'),
      '#default_value' => $this->getSetting('cell_processing'),
      '#options' => array(
        t('Plain text'),
        t('Filtered text (user selects input format)')
      ),
    );
    $element['default_message'] = array(
      '#type' => 'markup',
      '#value' => t('To specify a default table, use the &quot;Default Value&quot; above. There you can specify a default number of rows/columns and values.'),
    );

    return $element;
  }


}
